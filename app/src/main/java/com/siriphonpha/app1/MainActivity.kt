package com.siriphonpha.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.siriphonpha.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    //เพิ่มในส่วนของ main
    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding
    private lateinit var listIntent: Intent
    //ถึงตรงนี้
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.

        setContentView(R.layout.activity_main)
        //เพิ่มส่วนนี้ navHoast เป็นปุ่มกลับหน้าก่อนหน้า
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)
        //ถึงตรงนี้
    }
    //เพิ่มส่วนนี้ override onsupport
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    //ถึงตรงนี้

    private fun ProfileFragment() {
        listIntent = Intent(this, GridListActivity::class.java)
        startActivity(listIntent)
    }
}